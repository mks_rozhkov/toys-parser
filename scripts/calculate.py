from datetime import date

from main.models import *


class Calculator:
    """Класс для вычисления значений средней и минимальной цены для модели Statistic"""

    def __init__(self, lego_set):
        self.lego_set = lego_set
        self.minimal_price = None
        self.middle_price = None
        self.minimal_price_date = None
        self.today_price = None
        self.today_discount = None

    def calculate(self):
        """Вычисляет значения средней и минимальной цены на основе данных о ценах из БД"""
        all_prices = self.lego_set.prices.all()
        all_prices_list = all_prices.values()
        prices_dict = {}

        for price_entry in all_prices_list:
            date_str = price_entry['created_at']
            if date_str not in prices_dict:
                prices_dict[date_str] = price_entry['discount_price']
            else:
                prices_dict[date_str] = min(prices_dict[date_str], price_entry['discount_price'])

        self.minimal_price = min(prices_dict.values())
        reversed_prices_dict = {value: key for (key, value) in sorted(prices_dict.items())}
        self.minimal_price_date = reversed_prices_dict.get(self.minimal_price)
        self.middle_price = sorted(prices_dict.values())[len(prices_dict) // 2]
        self.today_price = prices_dict.get(date.today())
        self.today_discount = int((1 - self.today_price / self.middle_price) * 100) if self.today_price else None

    def save(self):
        """Сохраняет вычисленные значения в БД"""
        statistic, created = Statistic.objects.get_or_create(lego_set=self.lego_set)
        statistic.minimal_price = self.minimal_price
        statistic.middle_price = self.middle_price
        statistic.minimal_price_date = self.minimal_price_date
        statistic.today_price = self.today_price
        statistic.today_discount = self.today_discount
        statistic.save()

    def __str__(self):
        return f'{self.lego_set.id}: min {self.minimal_price_date} = {self.minimal_price}, mid = {self.middle_price}, today = {self.today_price}'


def run():
    for lego_set in LegoSet.objects.all():
        calculator = Calculator(lego_set)
        calculator.calculate()
        calculator.save()
        print(calculator)
