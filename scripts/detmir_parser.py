import re
from scripts.main_parser import Parser, ParsedPrice
# import sys

# sys.path.append('E:\DjangoProjects\\toyprices\scripts')


class DetmirParser(Parser):
    """Класс-парсер магазина Детский Мир"""
    link_pattern = 'https://www.detmir.ru/catalog/index/name/lego/page/'
    links_count = 25
    pattern_num = r'\d{4,5}'
    pattern_price = r'\d+\s*\d+'

    def parse_all_pages(self):
        try:
            for n in range(1, self.links_count + 1):
                print(f'страница {n} из {self.links_count}')
                self.get_html_chrome(self.link_pattern + str(n))
                self.get_tags('a', href=True)
                self._parse_one_page()
        finally:
            self.browser.quit()

    def _parse_one_page(self):
        for tag in self.tags_soup:
            try:
                if 'product/index/id' in str(tag['href']):
                    name_from_tag = tag.find('p', string=re.compile('LEGO'))
                    name = str(name_from_tag.contents[0]).replace('Конструктор LEGO ', '')
                    lego_set_id = int(max(re.findall(self.pattern_num, name), key=len))
                    link_url = tag['href']
                    full_price_from_tag = tag.find('span', string=re.compile('₽'))
                    discount_price_from_tag = tag.find('p', string=re.compile('₽'))
                    full_price = discount_price = None
                    if full_price_from_tag:
                        full_price = int(
                            re.search(self.pattern_price, full_price_from_tag.contents[0]).group(0).replace(' ', ''))
                    if discount_price_from_tag:
                        discount_price = int(
                            re.search(self.pattern_price, discount_price_from_tag.contents[0]).group(0).replace(' ', ''))
                    if not full_price:
                        full_price = discount_price
                    if not discount_price:
                        discount_price = full_price
                    if name and lego_set_id and discount_price and full_price:
                        parsed_data = ParsedPrice(lego_set_id=lego_set_id, name=name, shop_id=1, full_price=full_price,
                                                  discount_price=discount_price, link_url=link_url)
                        parsed_data.save_link()
                        if parsed_data.save_prices():
                            self.count += 1
            except ValueError as VE:
                print(VE, tag)
        print(f'всего добавлено цен: {self.count}')


def run():
    running_parser = DetmirParser()
    running_parser.parse_all_pages()
