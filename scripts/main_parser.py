import time
from datetime import date

from selenium import webdriver
from bs4 import BeautifulSoup

from django.conf import settings
from main.models import *


class Parser:
    """Класс-шаблон, содержащий общие методы для парсинга"""
    count = 0
    html_code = None
    tags_soup = None

    def __init__(self):
        self.today = date.today()

        if settings.DEBUG:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--headless')
            self.browser = webdriver.Chrome(options=chrome_options)
        else:
            firefox_options = webdriver.FirefoxOptions()
            firefox_options.headless = True
            self.browser = webdriver.Firefox(service_log_path="/home/mks/toyprices/geckodriver.log",
                                             options=firefox_options)

    def get_html_chrome(self, link_to_parse: str):
        self.browser.get(link_to_parse)
        time.sleep(1)
        self.html_code = self.browser.page_source

    def get_tags(self, *args, **kwargs):
        soup = BeautifulSoup(self.html_code, "html.parser")
        self.tags_soup = soup.find_all(*args, **kwargs)


class ParsedPrice:
    """Класс, для проверки и сохранения в БД данных о ценах и ссылках на конкретный набор"""

    def __init__(self, lego_set_id: int, name: str, shop_id: int, full_price: int = None,
                 discount_price: int = None, link_url: str = None):
        self.shop_id = shop_id
        self.full_price = full_price
        self.discount_price = discount_price
        self.link_url = link_url
        print(f'{lego_set_id}, {self.full_price}, {self.discount_price}, {self.link_url}')

        self.lego_set, created = LegoSet.objects.get_or_create(id=lego_set_id)
        if created:
            all_lego_series = [serie.name for serie in Serie.objects.all()]
            for serie_name in all_lego_series:
                if serie_name.lower() in name.lower():
                    self.lego_set.serie = Serie.objects.get(name=serie_name)
            self.lego_set.name = name
            self.lego_set.save()
            print('added set =', self.lego_set.name)

    def check_price(self, new_price: int):
        return new_price < min(self.discount_price, self.full_price)

    def save_prices(self):
        price, created = Price.objects.get_or_create(created_at=date.today(),
                                                     lego_set_id=self.lego_set.id, shop_id=self.shop_id)
        if created:
            price.full_price = self.full_price
            price.discount_price = self.discount_price
            price.shop_id = self.shop_id
            price.save()
            print(f'added price {price.shop.name}, {price.lego_set.id}, {price.full_price}, {price.discount_price}')
            return True
        else:
            return False

    def save_link(self):
        link, created = Link.objects.get_or_create(shop_id=self.shop_id, lego_set_id=self.lego_set.id)
        if created or Link.url != self.link_url:
            link.url = self.link_url
            link.save()
