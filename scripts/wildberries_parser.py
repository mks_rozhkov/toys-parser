import re

from bs4 import BeautifulSoup

from scripts.main_parser import Parser, ParsedPrice


class WildberriesParser(Parser):
    """
    Класс-парсер магазина Wildberries. Так как в магазине
    может быть несколько предложений по каждому набору ЛЕГО,
    данные сначала наполняют словарь temp_legosets_dict
    и только потом сохраняются в БД
    """

    product_card_classes = {'discount_price': [['span'], {'class_': 'price-block__final-price'}],
                            'full_price': [['del'], {'class_': 'price-block__old-price'}],
                            'description': [['p'], {'class_': 'collapsable__text'}]}
    link_pattern = 'https://www.wildberries.ru/brands/lego/all?sort=popular&page='
    links_count = 5
    pattern_num = r' \d{4,5} '
    pattern_price = r'(\d*\s*\d*\s)₽'
    pattern_name = r'>(.+)<'
    banned_legosets = ('2020', 2020)
    temp_legosets_dict = {}  # словарь, потому что на WB может быть несколько предложений по одному набору

    def parse_all_pages(self):
        product_count = 0
        try:
            for n in range(1, self.links_count + 1):
                self.get_html_chrome(self.link_pattern + str(n))
                print(self.link_pattern + str(n))
                self.get_tags('a', class_='product-card__main')
                product_count += self._parse_one_page()
                print(product_count)
        finally:
            self.browser.quit()
            print('browser closed')

    def _parse_one_page(self):
        product_count = 0
        for tag in self.tags_soup:
            print(f"{tag['href']}")
            name = lego_set_id = discount_price = full_price = None
            link_url = tag['href']
            self.browser.get(link_url)
            product_page_code = self.browser.page_source
            product_page_soup = BeautifulSoup(product_page_code, "html.parser")
            try:
                name_from_tag = product_page_soup.find('span', string=re.compile('Конструктор LEGO'))
                name = str(name_from_tag.contents[0]).replace('Конструктор LEGO ', '')
                lego_set_id = int(max(re.findall(self.pattern_num, name), key=len))
                print(f'{lego_set_id} ::: {name}')
                for class_tags in self.product_card_classes:
                    finded_tag = product_page_soup.find(*self.product_card_classes[class_tags][0],
                                                        **self.product_card_classes[class_tags][1])
                    # print(finded_tag)
                    if class_tags == 'discount_price':
                        discount_price_temp = str(re.search(self.pattern_price, finded_tag.string).group())
                        discount_price = int(re.sub(r'[\D]', '', discount_price_temp))
                        print(f'discount_price = {discount_price}')
                    elif class_tags == 'full_price':
                        if finded_tag:
                            full_price_temp = str(re.search(self.pattern_price, finded_tag.string).group())
                            full_price = int(re.sub(r'[\D]', '', full_price_temp))
                            print(f'full_price = {full_price}')
                        else:
                            full_price = discount_price
                    elif class_tags == 'description':
                        description = finded_tag.get_text()
                        if not description:
                            finded_tag = product_page_soup.find_all(*self.product_card_classes[class_tags][0],
                                                                    **self.product_card_classes[class_tags][1])
                            description = finded_tag[-1].get_text()
                        print(f'description = {len(description)} symbols')
                if name and lego_set_id and discount_price and full_price:
                    if not self.temp_legosets_dict.get(lego_set_id):
                        self.temp_legosets_dict[lego_set_id] = ParsedPrice(lego_set_id=lego_set_id, name=name,
                                                                           shop_id=2, full_price=full_price,
                                                                           discount_price=discount_price,
                                                                           link_url=link_url)
                        product_count += 1
                        print(f'Всего наборов --- {product_count} ---')
                    elif self.temp_legosets_dict[lego_set_id].check_price(discount_price):
                        self.temp_legosets_dict[lego_set_id].discount_price = discount_price
                        self.temp_legosets_dict[lego_set_id].full_price = full_price
            except Exception as E:
                print('84 line: ', E)
        return product_count

    def save_all_prices(self):
        for lego_set in self.temp_legosets_dict:
            if self.temp_legosets_dict[lego_set].save_prices():
                self.temp_legosets_dict[lego_set].save_link()
                self.count += 1
        print(f'всего добавлено {self.count}')


def run():
    running_parser = WildberriesParser()
    running_parser.parse_all_pages()
    running_parser.save_all_prices()
