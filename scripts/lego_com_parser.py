from typing import Optional

import requests
import os

from bs4 import BeautifulSoup
from django.db.models import Q
from django.core.files.base import File
from PIL import Image as PImage
from main.models import LegoSet, Image


images_folder = '/home/mks/toyprices/static/main/images/parsed/'


class LegoComParser:
    soup = details = ages = description = image_link = image_path = None
    page_template = 'https://www.lego.com/ru-ru/product/'
    ages_attrs = {'data-test': "product-details__ages"}
    details_attrs = {'data-test': "product-details__piece-count"}
    description_attrs = {'class': "ProductFeaturesstyles__FeaturesText-sc-8zwtdh-2"}
    image_link_attrs = {'class': "ProductFeaturesstyles__Cta-sc-8zwtdh-4"}

    def __init__(self, lego_set_id: int):
        self.lego_set_id = lego_set_id
        self.link = self.page_template + str(lego_set_id)

    def _get_soup_of_tags(self):
        html = requests.get(self.link)
        html.encoding = 'utf-8'
        self.soup = BeautifulSoup(html.text, "html.parser")

    def _find_ages_in_soup(self):
        temp_data = self.soup.find(attrs=self.ages_attrs)
        if temp_data:
            self.ages = temp_data.find(text=True)
            print(f'{self.ages=}')
        else:
            print(f'{self.lego_set_id} - ages not found')

    def _find_details_in_soup(self):
        temp_data = self.soup.find(attrs=self.details_attrs)
        if temp_data:
            self.details = temp_data.find(text=True)
            print(f'{self.details=}')
        else:
            print(f'{self.lego_set_id} - details not found')

    def _find_description_in_soup(self):
        temp_data = self.soup.find(attrs=self.description_attrs)
        if temp_data:
            self.description = temp_data.find(text=True)
            print(f'{self.description=}')
        else:
            print(f'{self.lego_set_id} - description not found')

    def _find_image_link_in_soup(self):
        temp_data = self.soup.find(attrs=self.image_link_attrs)
        if temp_data:
            image = temp_data.find('img')
            image_link = image.get('src')
            self.image_link = image_link.replace('320', '1500')
            print(f'{self.image_link=}')
        else:
            print(f'{self.lego_set_id} - image_link not found')

    def parse_all_data(self):
        self._get_soup_of_tags()
        self._find_ages_in_soup()
        self._find_details_in_soup()
        self._find_description_in_soup()
        self._find_image_link_in_soup()


class DBWriter:
    def __init__(self, lego_set_id: int):
        self.lego_set = LegoSet.objects.get(id=lego_set_id)

    def _save_image_from_url(self, image_path, image_link):
        new_file = open(image_path, "wb")
        image = requests.get(image_link)
        new_file.write(image.content)
        new_file.close()

    def _save_resized_image(self, existing_image_path, new_image_path):
        img = PImage.open(existing_image_path)
        width, height = img.size
        print(f"{width=}, {height=}")
        max_value = max(width, height)
        if max_value == width:
            new_size = (300, int(300 * width / height))
        else:
            new_size = (300, int(300 * height / width))
        print(f"{new_size=}")
        img.thumbnail(new_size)
        img.save(new_image_path)

    def check_image_and_save(self, image_link: str):
        if (not self.lego_set.images.all()) and image_link:
            big_image_path = str(images_folder) + 'parsed\\' + str(self.lego_set.id) + '-1500.jpg'
            if not os.path.isfile(big_image_path):
                self._save_image_from_url(big_image_path, image_link)
                print(big_image_path, 'saved')
            small_image_path = str(images_folder) + 'parsed\\' + str(self.lego_set.id) + '-300.jpg'
            if not os.path.isfile(small_image_path):
                self._save_resized_image(big_image_path, small_image_path)
                print(small_image_path, 'saved')
            with open(big_image_path, 'rb') as f:
                image = Image(type='MX', lego_set=self.lego_set)
                image.filename.save(name=str(self.lego_set.id) + '-1500px' + '.jpg', content=File(f))
                print(f"saved to DB {image.type=}, {image.image_path=}, {image.filename=}")
            with open(small_image_path, 'rb') as f:
                image = Image(type='MN', lego_set=self.lego_set)
                image.filename.save(name=str(self.lego_set.id) + '-300px' + '.jpg', content=File(f))
                print(f"saved to DB {image.type=}, {image.image_path=}, {image.filename=}")
            os.remove(big_image_path)
            os.remove(small_image_path)

    def _get_ages_formatted(self, ages_str: str) -> list[Optional[int]]:
        ages = ages_str.split('-')
        for i in range(len(ages)):
            ages[i] = int(ages[i].strip('+, ½'))
        #  Особенность IntegerRangeField - правая граница не включается
        if len(ages) == 2:
            ages[1] += 1
        elif len(ages) == 1:
            ages.append(None)
        return ages

    def save_data(self, details: Optional[int], ages: Optional[str], description: Optional[str]):
        if (not self.lego_set.details) and details:
            print(f'saved {details=}')
            self.lego_set.details = details
        if (not self.lego_set.ages) and ages:
            print(f'saved {ages=}')
            self.lego_set.ages = self._get_ages_formatted(ages)
        if (not self.lego_set.description) and description:
            print(f'saved {description=}')
            self.lego_set.description = description
        self.lego_set.save()


legosets_without_info = [legoset.id for legoset in LegoSet.objects.filter(Q(ages__isnull=True) |
                                                                          Q(description__isnull=True) |
                                                                          Q(details__isnull=True) |
                                                                          Q(description__exact=''))]
legosets_without_img = [legoset.id for legoset in LegoSet.objects.filter(images=None)]
legosets_without_any = set(legosets_without_info) | set(legosets_without_img)


def run():
    print(f'{len(legosets_without_any)=}')
    for lego_set in legosets_without_any:
        print(f'\n{lego_set=}')
        parser = LegoComParser(lego_set)
        parser.parse_all_data()
        writer = DBWriter(lego_set)
        writer.check_image_and_save(parser.image_link)
        writer.save_data(details=parser.details, ages=parser.ages, description=parser.description)
