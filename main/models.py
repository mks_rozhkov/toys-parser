from django.db import models
from django.contrib import admin
from django.contrib.postgres.fields import IntegerRangeField


class LegoSet(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=250, blank=True)
    serie = models.ForeignKey('Serie', blank=True, null=True, on_delete=models.DO_NOTHING, related_name='lego_sets')
    description = models.TextField(blank=True)
    ages = IntegerRangeField(blank=True, null=True)
    details = models.IntegerField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)  # TODO после импорта БД добавить auto_now_add=True

    def get_ages_str(self) -> str:
        """Возвращает значение ages (IntegerRangeField) в читаемом формате строки"""
        if self.ages:
            ages_list = [i for i in str(self.ages).strip('\'[)').split(', ') if i != 'None']
            if len(ages_list) == 2:
                # Особенность IntegerRangeField - правая граница не включается
                ages_list[1] = str(int(ages_list[1]) - 1)
                ages_str = ' - '.join(ages_list)
            else:
                ages_str = ages_list[0] + '+'
            return ages_str
        else:
            return 'нет информации'

    @admin.display(boolean=True)
    def image_exists(self):
        return self.images.exists()

    class Meta:
        db_table = 'lego_sets'
        verbose_name = 'Набор LEGO'
        verbose_name_plural = 'Наборы LEGO'

    def __str__(self):
        return self.name


class Price(models.Model):
    lego_set = models.ForeignKey('LegoSet', on_delete=models.CASCADE, related_name='prices')
    shop = models.ForeignKey('Shop', on_delete=models.CASCADE)
    # Цена без скидки
    full_price = models.IntegerField(blank=True, null=True)
    # Цена со кидкой
    discount_price = models.IntegerField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)  # TODO после импорта БД добавить auto_now_add=True

    class Meta:
        db_table = 'prices'
        verbose_name = 'Цена'
        verbose_name_plural = 'Цены'

    def __str__(self):
        return str(self.created_at) + ' ' + str(self.shop) + ' - ' + str(self.lego_set_id)


class Shop(models.Model):
    name = models.CharField(max_length=50)
    domain = models.CharField(max_length=150)
    lego_url = models.CharField(max_length=500)

    class Meta:
        db_table = 'shops'
        verbose_name = 'Мазазин'
        verbose_name_plural = 'Магазины'

    def __str__(self):
        return str(self.name)


class Serie(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'series'
        verbose_name = 'Серия LEGO'
        verbose_name_plural = 'Серии LEGO'

    def __str__(self):
        return self.name


class Statistic(models.Model):
    lego_set = models.OneToOneField('LegoSet', on_delete=models.CASCADE)
    minimal_price = models.IntegerField(blank=True, null=True)
    minimal_price_date = models.DateField(null=True)
    middle_price = models.IntegerField(blank=True, null=True)
    today_price = models.IntegerField(blank=True, null=True)
    today_discount = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'statistics'
        verbose_name = 'Статистика цен'
        verbose_name_plural = 'Статистика цен на набор'

    def __str__(self):
        return self.lego_set.name


class Link(models.Model):
    url = models.CharField(max_length=500)
    lego_set = models.ForeignKey('LegoSet', on_delete=models.CASCADE, related_name='links')
    shop = models.ForeignKey('Shop', on_delete=models.CASCADE, related_name='links')

    class Meta:
        db_table = 'links'
        verbose_name = 'Ссылка'
        verbose_name_plural = 'Ссылки'


class Image(models.Model):
    class ImageType(models.TextChoices):
        MAX = 'MX', 'Большое изображение'
        MIN = 'MN', 'Маленькое изображение'

    type = models.CharField(
        max_length=2,
        choices=ImageType.choices,
        blank=True
    )

    image_path = 'static/main/images'
    filename = models.ImageField(upload_to=image_path, blank=True, null=True)
    lego_set = models.ForeignKey('LegoSet', on_delete=models.CASCADE, related_name='images')

    class Meta:
        db_table = 'images'
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
