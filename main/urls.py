from django.urls import path
from . import views

urlpatterns = [
    path('', views.NewSetsListView.as_view(), name='index'),
    path('hot', views.HotPricesView.as_view(), name='hot'),
    path('all', views.LegoSetsListView.as_view(), name='all_lego_sets'),
    path('<int:pk>', views.LegoSetDetailView.as_view(), name='lego_set_info'),
    path('search', views.search, name='search'),
    path('search_by_name', views.SearchSetsListView.as_view(), name='search_by_name'),
    path('series/<int:pk>', views.SerieSetsListView.as_view(), name='serie_lego_sets'),
    path('series', views.LegoSeries.as_view(), name='series'),
]
