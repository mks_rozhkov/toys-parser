from .models import *


class MyAdminSite(admin.AdminSite):
    site_header = 'Панель администратора TOYPRICES.RU'


class LinksInline(admin.TabularInline):
    model = Link
    max_num = 2


class ImagesInline(admin.TabularInline):
    model = Image
    min_num = 2
    max_num = 2
    extra = 0


class ImageExistsListFilter(admin.SimpleListFilter):
    title = 'Изображения'

    parameter_name = 'image_exists'

    def lookups(self, request, model_admin):
        return ('yes', 'Есть изображения'), ('no', 'Нет изображений'),

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(images__isnull=False).distinct()
        if self.value() == 'no':
            return queryset.filter(images__isnull=True)


class LegoSetAdmin(admin.ModelAdmin):
    fields = ('id', 'name', 'serie', 'ages', 'details', 'description',)
    list_display = ('id', 'name', 'serie', 'ages', 'details', 'image_exists',)
    list_filter = (ImageExistsListFilter, 'serie')  # 'image_exists',
    search_fields = ('name', 'id', )
    inlines = (ImagesInline, LinksInline)


admin_site = MyAdminSite()
admin_site.register(LegoSet, LegoSetAdmin)
admin_site.register(Price)
admin_site.register(Shop)
admin_site.register(Serie)
admin_site.register(Statistic)
admin_site.register(Link)
