from datetime import date
from typing import Optional

from django.contrib.postgres.search import TrigramSimilarity
from django.db.models import Count
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView
from django.db.models import Q
from django.db.models import Prefetch
from psycopg2.extras import NumericRange

from .models import LegoSet, Price, Serie, Image


class LegoSetDetailView(DetailView):
    """Вывод всех свойств объекта LegoSet"""
    model = LegoSet
    # получаем все связанные объекты из БД
    queryset = LegoSet.objects.prefetch_related('prices').prefetch_related('links').prefetch_related(
        'statistic').prefetch_related(Prefetch('images', queryset=Image.objects.filter(type='MX'),
                                               to_attr='big_image'))
    template_name = 'main/lego_set_detailview.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ages'] = self.object.get_ages_str()
        context['prices'] = self._get_ordered_prices()
        context['today_prices'], context['today_minprice'] = self._get_today_prices()
        return context

    def _get_ordered_prices(self) -> list[list]:
        """Возвращает список списков, отсортированный по датам
        [date, shop1_full, shop1_disc, shop2_full, shop2_disc][...]"""
        prices_dict = {}
        for price_entry in self.object.prices.all():
            if str(price_entry.created_at) not in prices_dict:
                prices_dict[str(price_entry.created_at)] = [''] * 4
            prices_dict[str(price_entry.created_at)][price_entry.shop.id * 2 - 2] = price_entry.full_price
            prices_dict[str(price_entry.created_at)][price_entry.shop.id * 2 - 1] = price_entry.discount_price
        prices_list = [[date_value] + prices_dict[date_value] for date_value in sorted(prices_dict, reverse=True)]
        return prices_list

    def _get_today_prices(self) -> tuple[dict, Optional[int]]:
        """Возвращает словарь {shop_id: today_price} и цену продажи на сегодня"""
        today_prices_queryset = self.object.prices.filter(created_at=date.today())
        today_prices_dict = {
            price.shop.id: (min(price.full_price, price.discount_price) if price.full_price and price.discount_price
                            else None) for price in today_prices_queryset
        }
        today_minprice = min([price for price in today_prices_dict.values()]) if today_prices_dict else None
        return today_prices_dict, today_minprice


class LegoSetsListView(ListView):
    """Универсальный ListView для наборов"""

    model = LegoSet
    template_name = 'main/lego_sets_listview.html'
    paginate_by = 20
    ordering = ['id']

    # Значения по умолчанию для фильтров
    price_from = 0
    price_to = 50000
    age_from = 0
    age_to = 21
    details_from = 0
    details_to = 5000

    def get_queryset(self):
        # Определяем параметр pagination
        pagination = self.request.GET.get('pag')
        pagination_variants = ('20', '40', '60', '100')
        if pagination in pagination_variants:
            self.paginate_by = pagination
        else:
            self.paginate_by = 20

        # Определяем параметр ordering
        ordering = self.request.GET.get('ordering')
        # TODO прикрутить возврат в шаблон значений параметра, по которому производилась сортировка
        ordering_variants = {'pr': 'statistic__today_price', 'ds': 'statistic__today_discount',
                             'ag': 'ages', 'dt': 'details', 'nw': 'created_at',
                             '-pr': '-statistic__today_price', '-ds': '-statistic__today_discount',
                             '-ag': '-ages', '-dt': '-details', '-nw': '-created_at'}
        if ordering in ordering_variants.keys():
            # дополнительный параметр для того, чтобы объекты были отсортированы одинаково при одинаковых запросах
            self.ordering = [ordering_variants[ordering], 'id']

        queryset = super().get_queryset().prefetch_related('statistic').prefetch_related(
            Prefetch('images', queryset=Image.objects.filter(type='MN'), to_attr='small_image'))

        # Определяем параметры фильтрации:
        # цена от
        if self.request.GET.get('price_from') and self.request.GET.get('price_from').isdecimal() and \
                0 <= int(self.request.GET.get('price_from')) < 50000:
            self.price_from = int(self.request.GET.get('price_from'))
            queryset = queryset.filter(statistic__today_price__gt=self.price_from)

        # цена до
        if self.request.GET.get('price_to') and self.request.GET.get('price_to').isdecimal() and \
                0 <= int(self.request.GET.get('price_to')) < 50000:
            self.price_to = int(self.request.GET.get('price_to'))
            queryset = queryset.filter(statistic__today_price__lt=self.price_to)

        # возраст от до
        if self.request.GET.get('age_from') and self.request.GET.get('age_from').isdecimal() and \
                0 <= int(self.request.GET.get('age_from')) <= 18 and self.request.GET.get('age_to') \
                and self.request.GET.get('age_to').isdecimal() and 0 <= int(self.request.GET.get('age_to')) <= 18:
            age_from = int(self.request.GET.get('age_from'))
            age_to = int(self.request.GET.get('age_to')) + 1
            ages_filter = NumericRange(age_from, age_to)
            queryset = queryset.filter(Q(ages__overlap=ages_filter) &
                                       Q(ages__gte=NumericRange(age_from, None), ages__upper_inf=True))

        # деталей от
        if self.request.GET.get('details_from') and self.request.GET.get('details_from').isdecimal() and \
                0 <= int(self.request.GET.get('details_from')) < 5000:
            self.details_from = self.request.GET.get('details_from')
            queryset = queryset.filter(details__gt=self.details_from)

        # деталей до
        if self.request.GET.get('details_to') and self.request.GET.get('details_to').isdecimal() and \
                0 <= int(self.request.GET.get('details_to')) < 5000:
            self.details_to = int(self.request.GET.get('details_to'))
            queryset = queryset.filter(details__lt=self.details_to)

        # только со скидкой
        if self.request.GET.get('discount') and self.request.GET.get('discount') == 'on':
            queryset = queryset.filter(statistic__today_discount__gt=0)

        # изменение очередности queryset для того, чтобы значения None были в конце
        if self.ordering[0] == '-statistic__today_price':
            qs = queryset.annotate(null_position=Count('statistic__today_price')).order_by(
                '-null_position', '-statistic__today_price')
        elif self.ordering[0] == '-statistic__today_discount':
            qs = queryset.annotate(null_position=Count('statistic__today_discount')).order_by(
                '-null_position', '-statistic__today_discount')
        elif self.ordering[0] == 'ages' or self.ordering[0] == '-ages':
            qs = queryset
            for lego_set in qs:
                lego_set.ages_str = lego_set.get_ages_str()
        else:
            qs = queryset
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pagination'] = self.request.GET.get('pag')
        context['ordering'] = self.request.GET.get('ordering')
        context['page_title'] = "Все наборы LEGO"
        return context


class NewSetsListView(LegoSetsListView):
    """ListView для index (5 последних добавленных на сайт наборов)"""
    template_name = 'main/index.html'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.order_by('-created_at', '-id')[0:5]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lego_sets_count'] = LegoSet.objects.count()  # TODO снизить нагрузку на базу - занести в кэш?
        context['prices_count'] = Price.objects.count()  # TODO снизить нагрузку на базу - занести в кэш?
        return context


class HotPricesView(LegoSetsListView):
    """ListView для hot (Лучшие цены)"""

    def get_queryset(self):
        qs = super().get_queryset().filter(statistic__today_discount__gt=0)
        if self.ordering == ['id']:
            return qs.order_by('-statistic__today_discount', 'id')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Скидки на наборы LEGO"
        return context


class SearchSetsListView(LegoSetsListView):
    """ListView для search (Поиск наборов)"""
    paginate_by = None

    def get_queryset(self):
        # TODO deployment CREATE EXTENSION pg_trgm;
        qs = super().get_queryset()
        search_text = str(self.request.GET['search_request'])
        qs = qs.annotate(similarity=TrigramSimilarity('name', search_text)) \
            .filter(similarity__gt=0.05)
        if self.ordering == ['id']:
            return qs.order_by('-similarity')
        # TODO разобраться с параметром similarity, подобрать нужное значение
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search_request'] = str(self.request.GET['search_request'])
        context['page_title'] = "Поиск наборов " + str(self.request.GET['search_request'])
        return context


class SerieSetsListView(LegoSetsListView):
    """ListView для наборов из одной серии"""

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(serie_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Наборы LEGO из серии " + Serie.objects.get(id=self.kwargs['pk']).name
        return context


class LegoSeries(ListView):
    """ListView для серий"""
    model = Serie
    template_name = 'main/series.html'
    ordering = ['name']

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(lego_sets__serie__isnull=False).annotate(Count('id'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Все серии LEGO"
        return context


def search(request):
    """Функция для редиректа поискового запроса"""
    search_text = str(request.GET['search_request'])[:30]
    # попытка поиска по id
    if search_text.isdecimal():
        try:
            check_search_by_id = LegoSet.objects.get(id=search_text)
            if check_search_by_id:
                return redirect('lego_set_info', check_search_by_id.id)
        except LegoSet.DoesNotExist:
            pass
    # попытка поиска по названию
    return redirect('/search_by_name?search_request=' + search_text)  # TODO разобраться. Явно можно лучше!
