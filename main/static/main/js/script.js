window.onload = function () {
    // вызов модального окна с меню
    let menuButton = document.getElementById("header_js_bar");
    let modalMenu = document.getElementById("modal_menu");
    let pageWrapper = document.getElementById("all_page_wrapper");
    let closeMenuButton = document.getElementById("close_modal");
    menuButton.addEventListener('click', function () {
            pageWrapper.style.display = 'block';
            modalMenu.style.display = 'flex';
        }
    )
    closeMenuButton.addEventListener('click', function () {
        pageWrapper.style.display = 'none';
        modalMenu.style.display = 'none';
    })

    // переход по ссылке, вложенной в блок
    let allDivs = document.getElementsByClassName('div_href');
    for (let i = 0; i < allDivs.length; i++) {
        allDivs[i].addEventListener('click', function (e) {
            let productLink = allDivs[i].querySelector('a');
            if (e.target.matches(".form_submit")) {
                // alert('SEARCH')
                let searchForm = allDivs[i].querySelector("form");
                searchForm.submit()
            } else if (!e.target.matches('input')) {
                window.location.assign(productLink)
            }
        },true)
    }

    // перезагрузка при смене значения pagination
    let paginationSelect = document.getElementById('pagination-select');
    let params = new URLSearchParams(location.search);
    paginationSelect.addEventListener('change', function () {
        params.set('pag', this.value);
        let searchString = params.toString();
        window.location = location.protocol + '//' + location.host + location.pathname + '?' + searchString
    }, false);

    // опции сортировки в мобильной версии
    let sortingButtonOn = document.getElementById('expand_sorting');
    let sortingButtonOff = document.getElementById('remove_sorting');
    let sideBar = document.getElementsByClassName('sidebar_list')[0];
    sortingButtonOn.addEventListener('click', function () {
        sideBar.style.display = 'block';
        sortingButtonOff.style.display = 'block';
        sortingButtonOn.style.display = 'none';
    });
    sortingButtonOff.addEventListener('click', function () {
        sideBar.style.display = 'none';
        sortingButtonOff.style.display = 'none';
        sortingButtonOn.style.display = 'block';
    });
}
