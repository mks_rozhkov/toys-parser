$(function () {
    $("#slider-price-range").slider({
        range: true,
        min: 0,
        max: 30000,
        values: [0, 30000],
        slide: function (event, ui) {
            $("#price-from").val(ui.values[0]);
            $("#price-to").val(ui.values[1]);
        }
    });
    // $("#price-from").val($("#slider-price-range").slider("values", 0));
    // $("#price-to").val($("#slider-price-range").slider("values", 1));
});

$(function () {
    $("#slider-age-range").slider({
        range: true,
        min: 0,
        max: 18,
        values: [0, 18],
        slide: function (event, ui) {
            $("#age-from").val(ui.values[0]);
            $("#age-to").val(ui.values[1]);
        }
    });
    // $("#age-from").val($("#slider-age-range").slider("values", 0));
    // $("#age-to").val($("#slider-age-range").slider("values", 1));
});

$(function () {
    $("#slider-details-range").slider({
        range: true,
        min: 0,
        max: 5000,
        values: [0, 5000],
        slide: function (event, ui) {
            $("#details-from").val(ui.values[0]);
            $("#details-to").val(ui.values[1]);
        }
    });
    // $("#age-from").val($("#slider-age-range").slider("values", 0));
    // $("#age-to").val($("#slider-age-range").slider("values", 1));
});