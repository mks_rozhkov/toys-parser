from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.simple_tag
def paginator_url_replace(request, field, value):
    """Добавляет пагинацию к запросу с фильтрами"""
    dict_ = request.GET.copy()
    dict_[field] = value
    return dict_.urlencode()


@register.filter(name='dds')
@stringfilter
def delete_double_static(value):
    """Удаляет строку 'static/' из URL ссылок на изображения (путь к ним хранится в БД с указанием папки 'static/')"""
    return value.replace('static/', '')
